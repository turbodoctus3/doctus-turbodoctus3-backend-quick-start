using App.Infrastructure.Database.Context;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.AspNetCore.Hosting;
using IdentityServer4.AccessTokenValidation;
using System.Reflection;
using App.Config.DIAutoRegister;
using Doctus.Framework.NetCore.Cache;

namespace App.Config.Dependencies
{
    public class Container
    {
        public static void Register(IServiceCollection services, IConfiguration configuration)
        {

            #region Mapper

            services.AddMemoryCache();
            services.AddAutoMapper(typeof(Container));

            var configMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });

            var mapper = configMapper.CreateMapper();

            services.AddSingleton(mapper);

            #endregion

            #region Identity server

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = configuration["Authentication:Authority"];
                    options.Audience = configuration["Authentication:Audience"];
                    options.RequireHttpsMetadata = false;
                });

            #endregion

            #region Conexion Base de datos

            services.AddSingleton<IConfiguration>(configuration);

            services.AddScoped<TurbodoctusContext, TurbodoctusContext>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            #endregion

            #region RollBar
            #endregion

            #region Register DI
            var assembliesToScan = new[]
            {
                Assembly.GetExecutingAssembly(),
                Assembly.Load("App.Domain"),
                Assembly.Load("App.Infrastructure"),
                Assembly.Load("App.Common"),
                Assembly.Load("Doctus.Framework.NetCore")
            };

            services.RegisterAssemblyPublicNonGenericClasses(assembliesToScan)
                .Where(c => c.Name.EndsWith("Repository") ||
                       c.Name.EndsWith("Service") ||
                       c.Name.EndsWith("Validator") ||
                       c.Name.EndsWith("Resource"))
                .AsPublicImplementedInterfaces();
            #endregion

            #region Others
            services.AddSingleton<IConfiguration>(configuration);
            services.AddSingleton<IMemoryCacheManager, MemoryCacheManager>();
            #endregion

            #region Resources

            services.Configure<RequestLocalizationOptions>(opts =>
            {
                string english = "en-US";
                string spanish = "es";
                string spanishColombia = "es-CO";

                var supportedCultures = new List<CultureInfo> {
                    new CultureInfo(english),
                    new CultureInfo(spanish),
                    new CultureInfo(spanishColombia)
                };
                opts.DefaultRequestCulture = new RequestCulture(culture: english, uiCulture: english);
                opts.SupportedCultures = supportedCultures;
                opts.SupportedUICultures = supportedCultures;
            });

            #endregion
        }

        public static void CreateDatabase(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<TurbodoctusContext>();
                    DbInitializer.Initialize(context);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
