﻿namespace App.Infrastructure.Database.Context
{
    public static class DbInitializer
    {
        public static void Initialize(TurbodoctusContext context)
        {
            context.Database.EnsureCreated();
        }
    }

}
