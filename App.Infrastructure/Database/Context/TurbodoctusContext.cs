﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace App.Infrastructure.Database.Context
{
    public class TurbodoctusContext : DbContext
    {
        public IConfiguration _configuration { get; }

        #region DbSets

        #endregion

        public TurbodoctusContext() { }

        public TurbodoctusContext(DbContextOptions<TurbodoctusContext> options, IConfiguration configuration) : base(options)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("App.web"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }
    }
}