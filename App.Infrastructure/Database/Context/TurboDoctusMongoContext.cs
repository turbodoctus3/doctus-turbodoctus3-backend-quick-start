﻿using Doctus.Framework.NetCore.Base.Mongo;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace App.Infrastructure.Database.Context
{
    public class TurboDoctusMongoContext
    {
        private readonly IMongoDatabase _database = null;

        public TurboDoctusMongoContext(IOptions<MongoSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }
    }
}
