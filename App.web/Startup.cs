﻿using System;
using System.IO;
using System.Reflection;
using App.Config.Dependencies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using Rollbar;
using Microsoft.EntityFrameworkCore;
using App.Infrastructure.Database.Context;
using ElmahCore.Mvc;
using ElmahCore.Sql;
using Doctus.Framework.NetCore.Base.Exceptions;
using Doctus.Framework.NetCore.Services;
using Microsoft.Extensions.Localization;
using Doctus.Framework.NetCore.Resources;
using ElmahCore;

namespace App.web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var builder = new Microsoft.Extensions.Configuration.ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile("appsettings.json")
               .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
               .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Container.Register(services, Configuration);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //// ********************
            //// Setup CORS
            //// ********************
            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.AllowAnyHeader();
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyOrigin();
            corsBuilder.AllowCredentials();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", corsBuilder.Build());
            });

            services.Configure<FormOptions>(
               options =>
               {
                   options.MultipartBodyLengthLimit = long.MaxValue;
                   options.ValueLengthLimit = int.MaxValue;
                   options.MultipartHeadersLengthLimit = int.MaxValue;
               });

            services.AddDbContext<TurbodoctusContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("App.web"));
            });

            services.AddMvc()
                   .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                   .AddDataAnnotationsLocalization();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Turbodocuts",
                    Description = "Demo turbodoctus ASP.NET Core API",
                    TermsOfService = "None",

                });

                //importante para definir la documentacion de swagger
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                //importante para definir la documentacion de swagger
                c.IncludeXmlComments(xmlPath);
            });

            services.AddElmah<XmlFileErrorLog>(options =>
            {
                options.LogPath = "./elmahLog";
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IErrorService errorService, IStringLocalizer<GlobalResource> globalLocalizer)
        {

            app.UseDeveloperExceptionPage();
            app.UseMiddleware<ExceptionMiddleware>(errorService, globalLocalizer);

            app.UseCors("CorsPolicy");

            app.UseAuthentication();

            //app.UseRollbarMiddleware();

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Demo turbodoctus V1");
            });

            app.UseElmah();
        }

    }
}
