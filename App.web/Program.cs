﻿using App.Config.Dependencies;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace App.web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
            var host = BuildWebHost(args);
            Container.CreateDatabase(host);
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel()
                .Build();
    }
}
