﻿//using FakeItEasy;
//using Microsoft.EntityFrameworkCore;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace App.InfrastructureTest.Fixtures
//{
//    /// <summary>
//    /// Clase base para las entidades que realizarán operaciones de configuracion de 
//    /// datos para las pruebas unitarias
//    /// </summary>
//    public class BaseFixture : IDisposable
//    {
//        public BaseFixture() { }

//        /// <summary>
//        /// Crea los mocks necesarios para que el DbContext pueda realizar consultas 
//        /// sobre una tabla en especifico.
//        /// </summary>
//        /// <typeparam name="T">Clase que representa la tabla en base de datos donde se 
//        /// cargaran los datos de prueba</typeparam>
//        /// <param name="context">Contexto sobre el cual se van a cargar los datos</param>
//        /// <param name="dataSet">Datos de pruebas que se inyectaran en el contexto</param>
//        public void SetupDbSetForDbContext<T>(DbContext context, List<T> dataSet) where T : class
//        {
//            // Convertimos el listado en un IQueriable para poder acceder a los metodos 
//            // de esta interfaz
//            IQueryable<T> queryableDataSet = dataSet.AsQueryable();

//            // Creamos el mock del DbSet
//            DbSet<T> dbSet = A.Fake<DbSet<T>>(p => p.Implements<IQueryable<T>>());

//            // Mockeamos los metodos de la interfaz del IQueriable con los del dataSet
//            // de esta forma podemos hacer uso de Linq en nuestro DbSet
//            A.CallTo(() => ((IQueryable<T>)dbSet).Provider).Returns(queryableDataSet.Provider);
//            A.CallTo(() => ((IQueryable<T>)dbSet).Expression).Returns(queryableDataSet.Expression);
//            A.CallTo(() => ((IQueryable<T>)dbSet).ElementType).Returns(queryableDataSet.ElementType);
//            A.CallTo(() => ((IQueryable<T>)dbSet).GetEnumerator()).Returns(queryableDataSet.GetEnumerator());

//            // Mockeamos el metodo Set<T> el cual sera llamado por el DbContext para 
//            // inicializar la propiedad _Table, la cual es usada por los repositorios 
//            // para efectuar consultas
//            A.CallTo(() => context.Set<T>()).Returns(dbSet);
//        }

//        public void Dispose()
//        {

//        }
//    }
//}
