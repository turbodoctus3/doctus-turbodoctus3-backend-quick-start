﻿using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.DomainTest.Fixtures
{
    /// <summary>
    /// Clase base para las entidades que realizarán operaciones de configuracion de 
    /// datos para las pruebas unitarias
    /// </summary>
    public class BaseFixture : IDisposable
    {
        public BaseFixture() { }

        public void Dispose()
        {

        }
    }
}
