﻿using Doctus.Framework.NetCore.Resources;
using FakeItEasy;
using Microsoft.Extensions.Localization;
using System;

namespace App.DomainTest.Fixtures
{
    public abstract class BaseValidatorFixture<V, R> : BaseFixture
        where V : class
        where R : class
    {
        public IStringLocalizer<GlobalResource> GlobalLocalizer { get; set; }

        /// <summary>
        /// Genera los mocks para los casos de pruebas de las entidades.
        /// </summary>
        public BaseValidatorFixture()
        {
            this.GlobalLocalizer = A.Fake<IStringLocalizer<GlobalResource>>();

            //fixure para numeros
            A.CallTo(() => GlobalLocalizer["GreaterThanMessage"]).Returns(new LocalizedString("GreaterThanMessage", "GreaterThanMessage"));
            A.CallTo(() => GlobalLocalizer["LessThanMessage"]).Returns(new LocalizedString("LessThanMessage", "LessThanMessage"));
            A.CallTo(() => GlobalLocalizer["PropertyNotNullMessage"]).Returns(new LocalizedString("PropertyNotNullMessage", "PropertyNotNullMessage"));
            A.CallTo(() => GlobalLocalizer["DecimalFormatCorrect"]).Returns(new LocalizedString("DecimalFormatCorrect", "DecimalFormatCorrect"));
            A.CallTo(() => GlobalLocalizer["PropertyGreatherThanOrEqualMessage"]).Returns(new LocalizedString("PropertyGreatherThanOrEqualMessage", "PropertyGreatherThanOrEqualMessage"));
            A.CallTo(() => GlobalLocalizer["PropertyLessThanOrEqualMessage"]).Returns(new LocalizedString("PropertyLessThanOrEqualMessage", "PropertyLessThanOrEqualMessage"));
            A.CallTo(() => GlobalLocalizer["InvalidRangeMessage"]).Returns(new LocalizedString("InvalidRangeMessage", "InvalidRangeMessage"));
            //fixure para alfanumericos
            A.CallTo(() => GlobalLocalizer["InvalidRangeLengthMessage"]).Returns(new LocalizedString("InvalidRangeLengthMessage", "InvalidRangeLengthMessage"));
            A.CallTo(() => GlobalLocalizer["PropertyMinLengthMessage"]).Returns(new LocalizedString("PropertyMinLengthMessage", "PropertyMinLengthMessage"));
            A.CallTo(() => GlobalLocalizer["PropertyMaxLengthMessage"]).Returns(new LocalizedString("PropertyMaxLengthMessage", "PropertyMaxLengthMessage"));
            //fixure para fechas
            A.CallTo(() => GlobalLocalizer["PropertyDateMinMessage"]).Returns(new LocalizedString("PropertyDateMinMessage", "PropertyDateMinMessage"));
            A.CallTo(() => GlobalLocalizer["PropertyDateMaxMessage"]).Returns(new LocalizedString("PropertyDateMaxMessage", "PropertyDateMaxMessage"));
            A.CallTo(() => GlobalLocalizer["PropertyDateRangeMessage"]).Returns(new LocalizedString("PropertyDateRangeMessage", "PropertyDateRangeMessage"));

        }

        /// <summary>
        ///  Metodo abastracto que permite retornar los Mocks validator
        /// </summary>
        /// <returns>NUeva instancia </returns>
        public virtual V GetValidatorMocks()
        {
            IStringLocalizer<R> Localizer = A.Fake<IStringLocalizer<R>>();
            return (V)Activator.CreateInstance(typeof(V), new object[] { Localizer, GlobalLocalizer });
        }
    }
}
